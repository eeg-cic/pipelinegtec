import csv
import os
import datetime

datafile = input("Enter raw data file name (including .csv): ")
if datafile[0] == "\"":
    datafile = datafile[1:-1]

responsefile = input("Enter response file name (including .csv): ")
if responsefile[0] == "\"":
    responsefile = responsefile[1:-1]

enddate = input("Enter date from edf file (dd.mm.yyyy): ")
endtime = input("Enter time from edf file (hh.mm.ssssss): ")

# # os.rename(timestampfile, timestampfile[0:-3]+'csv')

epoch = input("Enter epoch (in seconds): ")
epoch = int(epoch)

hz = 250
f = 1/hz

#############################################################################
######################### Time Syncing ######################################
#############################################################################

print ("Time syncing...")

# enddate = '13.03.2019'
# endtime = '18.27.141536'
endsec = endtime[0:-4]
endmillisec = endtime[-4:-1]

dt = datetime.datetime.strptime(enddate+'T'+endsec, '%d.%m.%YT%H.%M.%S')
ut = dt.timestamp()
unix_signal_endtime = (ut*1000) + int(endmillisec)

# Find unix start time of signal
# ifile = open('LG03DataEEGS2.csv', "r")
ifile = open(datafile, "r")
reader = csv.reader(ifile, delimiter = ';')
rowcount = sum(1 for row in ifile) - 2
ifile.close()
unix_signal_starttime = unix_signal_endtime - (rowcount * 4)

# Write new file Unix time + signal 8 channel
# ifile = open('LG03DataEEGS2.csv', "r")
ifile = open(datafile, "r")
reader = csv.reader(ifile, delimiter = ';')
ofile = open('TimeSignal.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

rownum = 0
for row in reader:
    if rownum > 0:
        tray = row[1:9]
        timemach = str(unix_signal_starttime+((rownum-1)*4))
        tray.insert(0,timemach)
        writer.writerow(tray)
    rownum+=1


ifile.close()
ofile.close()

# Read response file and collect in Python list response
# ifile = open('LG03DataGtechseanceindividuelle.csv', "r")
ifile = open(responsefile, "r")
reader = csv.reader(ifile, delimiter = ';')

response = []
tray = []

rownum = 0
for row in reader:
    if rownum > 0:
        time = int(float(row[1].replace(',','.')))
        tray = [time, row[2], row[3]]
        response.append(tray)
    rownum+=1

ifile.close()

# Insert Response into TimeSignal file
ifile = open('TimeSignal.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')
ofile = open('TimeSignalResponse.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

tray = []
rowresponse = 0
for row in reader:
    row.append('')
    row.append('')
    if rowresponse < len(response):
        if float(row[0]) > float(response[rowresponse][0]):
            tray = [response[rowresponse][0], '', '', '', '', '', '', '', '', response[rowresponse][1], response[rowresponse][2]]
            writer.writerow(tray)
            writer.writerow(row)
            rowresponse += 1
        else:
            writer.writerow(row)
    else:
            writer.writerow(row)

ifile.close()
ofile.close()

# Cut the beginning and the end
ifile = open('TimeSignalResponse.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')
ofile = open('TimeSignalResponseTrim.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

flag = 0
for row in reader:

    if row[10] != '':

        if float(row[10].replace(',','.')) == 1:
            flag = 2

        if float(row[10].replace(',','.')) == 8:
            flag = flag-1
            if flag == 0:
                writer.writerow(row)

    if flag == 0:
        pass
    else:
        writer.writerow(row)

ifile.close()
ofile.close()
# os.remove('TimeSignalResponse.csv')
# os.remove('TimeSignal.csv')

print ("Finished time sync...")

#############################################################################
######################### Delete Resp. Line #################################
#############################################################################

print ("Deleting response and music line...")

ifile = open("TimeSignalResponseTrim.csv", "r")
reader = csv.reader(ifile, delimiter = '\t')
ofile = open('OutputTemp.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

rownum = 0
scribble = [""]*11
backlog1 = [""]*11
backlog2 = [""]*11
backlog3 = [""]*11
for row in reader:
    scribble = backlog1
    backlog1 = backlog2
    backlog2 = backlog3
    backlog3 = row

    if backlog2[9] != "" and backlog2[10] == "":
        timebefore = float(backlog2[0].replace(',' , '.')) - float(backlog1[0].replace(',' , '.'))
        timeafter = float(backlog3[0].replace(',' , '.')) - float(backlog2[0].replace(',' , '.'))

        if timebefore<=timeafter:
            backlog1[9] = backlog2[9]
        else:
            backlog3[9] = backlog2[9]

    if scribble[1] == "" and scribble[10] == "":
        pass
    else:
        writer.writerow(scribble)
    
writer.writerow(backlog1)
writer.writerow(backlog2)
writer.writerow(backlog3)

ifile.close()
ofile.close()
# os.remove('TimeSignalResponseTrim.csv')

#############################################################################
########################### Delete Music Line ###############################
#############################################################################

ifile = open('OutputTemp.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')
ofile = open('Output1.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

rownum = 0
flag = 0
scribble = [""]*11
backlog1 = [""]*11
backlog2 = [""]*11
backlog3 = [""]*11
for row in reader:
    scribble = backlog1
    backlog1 = backlog2
    backlog2 = backlog3
    backlog3 = row

    if backlog2[10] != "" and backlog2[9] == "" and rownum > 1:
        timebefore = float(backlog2[0].replace(',' , '.')) - float(backlog1[0].replace(',' , '.'))
        timeafter = float(backlog3[0].replace(',' , '.')) - float(backlog2[0].replace(',' , '.'))

        if timebefore<=timeafter:
            backlog1[10] = backlog2[10]
        else:
            backlog3[10] = backlog2[10]

    if scribble[1] == "" and scribble[9] == "":
        if rownum == 3:
            writer.writerow(scribble)
        pass
    else:
        writer.writerow(scribble)

    rownum+=1
    
writer.writerow(backlog1)
writer.writerow(backlog2)
writer.writerow(backlog3)

ifile.close()
ofile.close()
# os.remove('OutputTemp.csv')

print ("Finished as Output1.csv")

#############################################################################
########################### Keep Only Signal ################################
#############################################################################

print ("Keeping only EEG signal...")

ifile = open('Output1.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')
ofile = open('Output2.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

row_count = sum(1 for row in ifile)
writer.writerow(["8", str(row_count-2), "250","","","","",""])

ifile = open('Output1.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')

rownum = 0

for row in reader:
    if rownum > 0 and rownum < row_count:
        writer.writerow(row[1:9])
    rownum+=1

ifile.close()
ofile.close()

print ("Finished as Output2.csv")

#############################################################################
########################### Prepare Marker ##################################
#############################################################################

print ("Preparing marker file...")

ifile = open('Output1.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')
ofile = open('Output3.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

rownum = 0
holder = 0
newer = 0

writer.writerow(["TL02"])

for row in reader:
    if rownum < 1:
        pass
    else:
        if row[9] != "":
            newer = float(row[9].replace(',' , '.'))
            if newer-holder == 0:
                pass
            else:
                if newer == 1:
                    text = "neut"
                elif newer == 2:
                    text = "low"
                elif newer == 3:
                    text = "high"
                elif newer == 4:
                    text = "chill"
                elif newer == 0:
                    text = "noresp"
                writer.writerow([str(rownum),str(rownum),text])
                holder=newer

    rownum += 1

ifile.close()
ofile.close()
print ("Finished as Output2.csv")

#############################################################################
##################### Prepare Additional Marker #############################
#############################################################################


print ("Preparing additional marker file...")

ifile = open('Output2.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')
row_count = sum(1 for row in reader) - 1
ifile.close()

ifile = open('Output3.csv', "r")
reader = csv.reader(ifile, delimiter = '\t')
ofile = open('Output4.csv', 'w', newline = "")
writer = csv.writer(ofile, delimiter = '\t')

rownum = 0
scribble = [""]*3
tray = [""]*3

for row in reader:
    scribble = tray
    tray = row
    #Process the rest
    if rownum > 1:
        writer.writerow(scribble)
        gap = int(tray[0]) - int(scribble[0])
        mark = int(gap/(hz*epoch))
        for i in range (mark):
            nextp = int(scribble[0])+((hz*epoch)*(i+1))
            placeholder = scribble[2] + "M"
            if placeholder != "norespM":
                writer.writerow([str(nextp),str(nextp),placeholder])



    #Skip header
    if rownum == 0:
        writer.writerow(row)

    rownum += 1

    
writer.writerow(tray)
gap = row_count - int(tray[0])
mark = int(gap/(hz*epoch))
for i in range (mark):
    nextp = int(tray[0])+((hz*epoch)*(i+1))
    placeholder = tray[2] + "M"
    writer.writerow([str(nextp),str(nextp),placeholder])


ifile.close()
ofile.close()
print ("Finished as Output4.csv")
print ("Deleting temp file...")
os.remove('OutputTemp.csv')
os.remove('TimeSignal.csv')
os.remove('TimeSignalResponse.csv')
os.remove('TimeSignalResponseTrim.csv')
print ("All operation finished.")
print ("Pipeline was closed.")